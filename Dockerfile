FROM debian:bookworm-slim

ARG DEBIAN_FRONTEND=noninteractive
ARG UNAME=cobalt
ARG UID=1000
ARG GID=1000

SHELL ["/bin/bash", "-c"]

ENV NVM_DIR=/opt/nvm
ENV NODE_VERSION=12.17.0
ENV RASPI_HOME=/opt/raspi-tools

RUN apt-get update && apt-get dist-upgrade -y && \
    apt install -qqy --no-install-recommends \
    pkgconf ninja-build bison yasm binutils clang libgles2-mesa-dev \
    mesa-common-dev libpulse-dev libasound2-dev \
    libxrender-dev libxcomposite-dev libxml2-dev curl git \
    python3-venv python2 g++-multilib \
    wget xz-utils libxml2  binutils-aarch64-linux-gnu \
    binutils-arm-linux-gnueabi  libglib2.0-dev \
	cpplint pre-commit pylint yapf3 python3-requests sudo

RUN groupadd -g $GID -o $UNAME
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME
RUN adduser $UNAME sudo
RUN echo "$UNAME:$UNAME" | chpasswd

RUN mkdir -p /etc/sudoers.d
RUN echo "%sudo	ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/cobalt
RUN chmod 0644 /etc/sudoers.d/cobalt

RUN curl https://bootstrap.pypa.io/pip/2.7/get-pip.py | python2
RUN python2 -m pip install requests selenium six

RUN mkdir -p $NVM_DIR $RASPI_HOME

RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.35.3/install.sh | bash

RUN source $NVM_DIR/nvm.sh && nvm install --lts && nvm alias default lts/* && nvm use default

RUN chown $UNAME.$UNAME $RASPI_HOME

USER $UNAME

WORKDIR $RASPI_HOME
RUN curl https://storage.googleapis.com/cobalt-static-storage/cobalt_raspi_tools.tar.bz2 | tar xjf -

WORKDIR /home/$UNAME
ENTRYPOINT /bin/bash
